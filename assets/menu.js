jQuery(document).ready(function($){

    $(document).click(function(event) {
        if(!$(event.target).closest('.click-menu > .menu-dropdown').length) {
            $('.click-menu').find('li.menu-dropdown.open').removeClass('open');
        }
    })

    $('.click-menu > li.menu-dropdown > a').click(function(e) {
        e.preventDefault();
        var outerMenu = $(this).closest('.click-menu .outer-menu');
        var parentNavItem = $(this).parent();

        parentNavItem.siblings().removeClass('open');

        if (parentNavItem.hasClass('open')) {
            parentNavItem.removeClass('open');
        } else {
            outerMenu.find('> li.menu-dropdown').removeClass('open');
            parentNavItem.addClass('open');
        }
        return false;
    });

    $('.click-menu .activity > a[data-handle]').click(function (e) {
        e.preventDefault();
        var menu = $(this).closest('.click-menu .outer-menu');
        var activityMenu = menu.find('.activity-menu');
        var parent = $(this).parent();

        if( !parent.hasClass('open') ) {
            activityMenu.find('> .activity').removeClass('open');
            parent.addClass('open');
            var handleToOpen = $(this).data('handle');

            var activityColumns = menu.find('.activity-column-set');
            activityColumns.removeClass('open');
            activityColumns.siblings('[data-parent-handle="' + handleToOpen + '"]').addClass('open');

        }

        return false;
    });

});